<?php

class BlockHelperTest extends Kybernaut\TestCase {

    public function testItWorks() {

        // \WP_Mock::userFunction('has_block', array(
        //     // 'times' => 1,
        //     'return' => 'true'
        // ));

        \WP_Mock::userFunction('has_block', [
            'args' => 'core/block',
            'return' => true,
        ]);
        \WP_Mock::userFunction('has_block', [
            'args' => 'core/heading',
            'return' => false,
        ]);

        $subject = new Kybernaut\BlockHelper();
        $this->assertTrue($subject->has_block('core/heading'));
        $this->assertTrue($subject->has_block('core/block'));
        $this->assertTrue($subject->has_block('core/test')); // no handler

    }

}

// willReturnMap
// willReturnCallback
// $this->createMock(); na has_block();
