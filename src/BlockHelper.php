<?php

namespace Kybernaut;

class BlockHelper
{

    private $blocks;

    /**
     * Has block implementation counting with reusable blocks
     *
     * @see https://github.com/WordPress/gutenberg/issues/18272
     * @param string $block_name Block name.
     * @return bool
     */
    public function has_block($block_name)
    {

        if (\has_block($block_name)) {
            return true;
        }

        $this->parse_reusable_blocks();

        if (empty($this->blocks)) {
            return false;
        }

        return $this->recursive_search_within_innerblocks($this->blocks, $block_name);
    }

    /**
     * Search for a reusable block inside innerblocks
     *
     * @param array $blocks Blocks to loop through.
     * @param string $block_name Block name to search for.
     * @return true|void
     */
    private function recursive_search_within_innerblocks($blocks, $block_name)
    {
        foreach ($blocks as $block) {
            if (isset($block['innerBlocks']) && !empty($block['innerBlocks'])) {
                $this->recursive_search_within_innerblocks($block['innerBlocks'], $block_name);
            } elseif ($block['blockName'] === 'core/block' && !empty($block['attrs']['ref'])) {
                if (\has_block($block_name, $block['attrs']['ref'])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Parse blocks if at leat one reusable block is presnt.
     *
     * @return void
     */
    private function parse_reusable_blocks()
    {

        if ($this->blocks !== null) {
            return;
        }

        if (\has_block('core/block')) {
            $content = \get_post_field('post_content');
            $this->blocks = \parse_blocks($content);
            return;
        }

        $this->blocks = false;
    }
}
